/*jslint undef:true*/
/*jslint nomen:true*/
var express = require('express');
var app = new express();

app.get('/', function (req, res) {
    'use strict';
    /*jslint undef: true*/
    /*jslint nomen:true*/
    res.sendFile(__dirname + "/assignment5b.html");
});
app.get('/assignment5-technical.html', function (req, res) {
    'use strict';
    /*jslint undef: true*/
    /*jslint nomen:true*/
    res.sendFile(__dirname + "/assignment5-technical.html");
});

app.get('/assignment5-nontechnical.html', function (req, res) {
    'use strict';
    /*jslint undef: true*/
    /*jslint nomen:true*/
    res.sendFile(__dirname + "/assignment5-nontechnical.html");
});

app.listen(8000, function () {
    'use strict';
    /*jslint undef: true*/
    /*jslint nomen:true*/
    console.log('Server listening to 8000');
});